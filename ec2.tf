data "aws_ami" "latest_amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "server_linux_amazon" {
  ami                    = data.aws_ami.latest_amazon_linux.id
  instance_type          = "t2.nano"
  vpc_security_group_ids = [module.security_groups.sg_id]
  subnet_id              = module.public-subnet.subnet_id
  user_data              = data.template_file.startup.rendered
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  tags = {
    Name = "Web-server"
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.server_linux_amazon.id
  allocation_id = aws_eip.eip.id
}

data "template_file" "startup" {
    template = file("startup.sh")
    vars = {
      bucket_name = aws_s3_bucket.bucket.id
    }
}