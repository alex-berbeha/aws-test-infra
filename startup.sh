#!/bin/bash
yum update -y
yum install -y httpd
aws s3 cp s3://${bucket_name}/index.html /var/www/html/index.html
systemctl start httpd
systemctl enable httpd