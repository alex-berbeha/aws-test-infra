resource "aws_s3_bucket" "bucket" {
  bucket = "temy-bucket-for-static-content"
  acl    = "private"
}